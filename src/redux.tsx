import { ReducersMapObject } from 'redux';
import { routerReducer } from 'react-router-redux';
import environment, { EnvironmentState } from './environment/Environment';
import errorState, { ErrorState } from './reducers/errors';
import login, { LoginState } from './reducers/loginReducer';

export const reducers: ReducersMapObject = {
    environment,
    errorState,
    login,
    router: routerReducer,
};

export interface AppState {
    environment: EnvironmentState;
    errorState: ErrorState;
    login: LoginState;
}

export const emptyAppState: () => AppState = () => ({
    environment: {},
    errorState: {},
    login: {
        username: '',
        password: ''
    }
});
