import { connect } from 'react-redux';
import Error from './Error';
import { Dispatch } from 'redux';
import { AppState } from '../redux';

export const mapStateToProps = (state: AppState) => {
    return {
        errorTitle: state.errorState.errorTitlePrimary
    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        onSubmit: () => {
            // dispatch(retryFromException);
        },
    };
};

const ErrorContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Error);

export default ErrorContainer;
