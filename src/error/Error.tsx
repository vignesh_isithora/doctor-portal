import * as React from 'react';
import ErrorPageView from '../components/layouts/ErrorPageView';

export interface ErrorProps {
    onSubmit: () => void;
    errorTitle: string;
}

class Error extends React.Component<ErrorProps> {
    render() {
        return (
            <ErrorPageView />
        );
    }
    componentWillMount() {
        // console.log("Test");
    }
}

export default Error;
