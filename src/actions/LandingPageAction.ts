import { Dispatch } from 'redux';
import { AppState } from '../redux';
// import { STORE_ERROR_MESSAGE, STORE_ERROR_BAR } from 'src/reducers/errors';
import { push } from 'react-router-redux';

export const navigationPage = (navPage: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        switch (navPage) {
            case 'login':
                dispatch(push('/login'));
                break;
            case 'home':
                dispatch(push('/app/home'));
                break;
            case 'newCase':
                dispatch(push('/app/newCase'));
                break;
            case 'draft':
                dispatch(push('/app/draft'));
                break;
            case 'live':
                dispatch(push('/app/live'));
                break;
            case 'ship':
                dispatch(push('/app/shipping'));
                break;
            case 'smile':
                dispatch(push('/app/smile'));
                break;
            case 'getLabel':
                dispatch(push('/app/getLabel'));
                break;
            case 'schedule':
                dispatch(push('/app/schedule'));
                break;
            case 'scan':
                dispatch(push('/app/scan'));
                break;
            case 'track':
                dispatch(push('/app/track'));
                break;
            case 'ups':
                dispatch(push('/app/ups'));
                break;
            case 'rating':
                dispatch(push('/app/rating'));
                break;
            case 'supplies':
                dispatch(push('/app/supplies'));
                break;
            case 'support':
                dispatch(push('/app/support'));
                break;
            case 'news':
                dispatch(push('/app/news'));
                break;
            case 'promotions':
                dispatch(push('/app/promotions'));
                break;
            case 'payment':
                dispatch(push('/app/payment'));
                break;
            case 'invoices':
                dispatch(push('/app/invoices'));
                break;
            case 'draftPayment':
                dispatch(push('/app/draftPayment'));
                break;
            case 'register':
                dispatch(push('/app/register'));
                break;
            case 'myCards':
                dispatch(push('/app/myCards'));
                break;
            case 'rxForm':
                dispatch(push('/app/rxForm'));
                break;
            case 'estimation':
                dispatch(push('/app/estimation'));
                break;
            case 'listReport':
                dispatch(push('/app/listReport'));
                break;
            case 'remakeReport':
                dispatch(push('/app/remakeReport'));
                break;
            case 'ratingReport':
                dispatch(push('/app/ratingReport'));
                break;
            case 'ticketReport':
                dispatch(push('/app/ticketReport'));
                break;
            default:
                console.error('Empty action received.');
                break;
        }
    };
};
