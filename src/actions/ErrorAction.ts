import { AnyAction, Dispatch } from 'redux';
import { AppState } from '../redux';
import { STORE_ERROR_BAR_PRIMARY, STORE_SUCCESS_BAR_PRIMARY } from 'src/reducers/errors';

export const handleCloseAlert = (dispatch: Dispatch<AppState>, getState: () => AppState) => {

    // const errorFileUploadedOpen: boolean = false;
    // const successFileUploadOpen: boolean = false;
    // dispatch({ type: STORE_SUCCESS_FILES_UPLOADED, successFileUploadOpen } as AnyAction);
    // dispatch({ type: STORE_ERROR_FILES_UPLOADED, errorFileUploadedOpen } as AnyAction);
    const errorBarPrimary: boolean = false;
    dispatch({ type: STORE_ERROR_BAR_PRIMARY, errorBarPrimary } as AnyAction);
    const successBarPrimary: boolean = false;
    dispatch({ type: STORE_SUCCESS_BAR_PRIMARY, successBarPrimary } as AnyAction);
};
