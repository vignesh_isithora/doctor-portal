import { Dispatch, AnyAction } from 'redux';
import { AppState } from '../redux';
import { STORE_ERROR_MESSAGE_PRIMARY, STORE_ERROR_BAR_PRIMARY, STORE_ERROR_TITLE_PRIMARY } from 'src/reducers/errors';
import { push } from 'react-router-redux';

// import axios, { AxiosResponse } from 'axios';

export const doAuthentication = (username: string, password: string) => {
    return async (
        dispatch: Dispatch<AppState>,
        getState: () => AppState
    ) => {
        const params = {
            userName: '',
            passWord: ''
        };
        params.userName = username;
        params.passWord = password;

        console.error(params, 'result');

        if (params.userName === '') {
            const errorTitlePrimary = 'Authorization Error';
            const errorBarPrimary = true;
            const errorMessagePrimary = 'Username should not be blank.';
            dispatch({ type: STORE_ERROR_TITLE_PRIMARY, errorTitlePrimary } as AnyAction);
            dispatch({ type: STORE_ERROR_MESSAGE_PRIMARY, errorMessagePrimary } as AnyAction);
            dispatch({ type: STORE_ERROR_BAR_PRIMARY, errorBarPrimary } as AnyAction);
            return;
        } else {
            const errorBarPrimary = false;
            dispatch({ type: STORE_ERROR_BAR_PRIMARY, errorBarPrimary } as AnyAction);
        }

        if (params.passWord === '') {
            const errorTitlePrimary = 'Authorization Error';
            const errorBarPrimary = true;
            const errorMessagePrimary = 'Password should not be blank.';
            dispatch({ type: STORE_ERROR_TITLE_PRIMARY, errorTitlePrimary } as AnyAction);
            dispatch({ type: STORE_ERROR_MESSAGE_PRIMARY, errorMessagePrimary } as AnyAction);
            dispatch({ type: STORE_ERROR_BAR_PRIMARY, errorBarPrimary } as AnyAction);
            return;
        } else {
            const errorBarPrimary = false;
            dispatch({ type: STORE_ERROR_BAR_PRIMARY, errorBarPrimary } as AnyAction);
        }

        dispatch(push('/app'));

        //   await axios({
        //     method: 'post',
        //     url: `${getState().environment.API_URL}login`,
        //     data: params,
        //   }).then(function (response: AxiosResponse) {

        //     if ((response.data.ugroup === 'Lab') || (response.data.ugroup === 'Manufacturer') || (response.data.ugroup === 'EDDL')) {

        //       const userDetails: UserDetail = {
        //         s: response.data.s,
        //         ugroup: response.data.ugroup,
        //         uroles: response.data.uroles,
        //         userName: response.data.USERNAME,
        //         userGroup: response.data.ugroup,
        //         groupNo: response.data.GROUPNO,
        //         email: response.data.EMAIL,
        //         nickName: response.data.NICKNAME,
        //         userType: response.data.USERTYPE,
        //         mobileNumber: response.data.MOBILE,
        //         allUserGroup: response.data.ALLUSERGROUP,
        //         responsibilies: response.data.globalvars.responsibilies,
        //         roleName: response.data.globalvars.rolename,
        //         labCode: response.data.globalvars.mcustomer
        //       };

        //       dispatch({ type: STORE_USER_PROFILE, userDetails } as AnyAction);

        //       const username = '';
        //       const password = '';
        //       dispatch({ type: STORE_USERNAME, username } as AnyAction);
        //       dispatch({ type: STORE_PASSWORD, password } as AnyAction);

        //       dispatch(push('/app'));
        //     } else {
        //       const errorBar = true;
        //       const errorMessage = 'You are not authorized to access this application';
        //       dispatch({ type: STORE_ERROR_MESSAGE, errorMessage } as AnyAction);
        //       dispatch({ type: STORE_ERROR_BAR, errorBar } as AnyAction);
        //     }

        //     return;
        //   }).catch(error => {

        //     const username = '';
        //     const password = '';
        //     dispatch({ type: STORE_USERNAME, username } as AnyAction);
        //     dispatch({ type: STORE_PASSWORD, password } as AnyAction);

        //     const errorMessage = error.response.data.message;
        //     const errorBar = true;
        //     dispatch({ type: STORE_ERROR_MESSAGE, errorMessage } as AnyAction);
        //     dispatch({ type: STORE_ERROR_BAR, errorBar } as AnyAction);

        //   });
    };
};
