import * as React from 'react';
import SupportLayout from '../layouts/SupportLayout';

export interface SupportLayoutProps {

}

class SupportComponent extends React.Component<SupportLayoutProps> {
    render() {
        return (
            <div>
                <SupportLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default SupportComponent;
