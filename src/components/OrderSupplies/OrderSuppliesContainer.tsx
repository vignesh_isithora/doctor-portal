import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import OrderSuppliesComponent from './OrderSuppliesComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const OrderSuppliesContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(OrderSuppliesComponent);

export default OrderSuppliesContainer;
