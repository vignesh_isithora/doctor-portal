import * as React from 'react';
import OrderSuppliesLayout from '../layouts/OrderSuppliesLayout';

export interface OrderSuppliesLayoutProps {

}

class OrderSuppliesComponent extends React.Component<OrderSuppliesLayoutProps> {
    render() {
        return (
            <div>
                <OrderSuppliesLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default OrderSuppliesComponent;
