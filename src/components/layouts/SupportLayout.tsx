import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';

export interface SupportLayoutProps {

}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
    })
);

const SupportLayout: React.StatelessComponent<SupportLayoutProps> = props => {
    const classes = useStyles({});

    return (
        <div className={classes.root}>
            <CssBaseline />
            <h1>Support Page</h1>
        </div>
    );
};

export default SupportLayout;
