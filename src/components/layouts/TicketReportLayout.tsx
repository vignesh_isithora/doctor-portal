import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from 'material-table';
import { Column } from 'material-table/types/index';
import { tableIcons } from './DraftCaseLayout';

interface Row {
    tickettype: string;
    ticketno: string;
    caseno: string;
    patientname: string;
    salesrep: string;
    groupname: string;
    receivedon: string;
    replyon: string;
    status: string;
}

interface TableState {
    columns: Array<Column<Row>>;
    data: Row[];
}

export interface TicketReportLayoutProps {

}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
    })
);

const TicketReportLayout: React.StatelessComponent<TicketReportLayoutProps> = props => {
    const classes = useStyles({});
    const [state] = React.useState<TableState>({
        columns: [
            {
                title: 'Ticket Type',
                field: 'tickettype',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Ticket No',
                field: 'ticketno',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Case No',
                field: 'caseno',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Patient Name',
                field: 'patientname',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Sales Rep',
                field: 'salesrep',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Group Name',
                field: 'groupname',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Received On',
                field: 'receivedon',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Last Reply On',
                field: 'replyon',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Status',
                field: 'status',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
        ],
        data: [
            {
                tickettype: '0025',
                ticketno: '0025',
                caseno: '0025',
                patientname: 'VENKATESH BABU',
                salesrep: 'BABU',
                groupname: 'VENKATESH',
                receivedon: '05/29/2020',
                replyon: '05/29/2020',
                status: 'Processing'
            },
            {
                tickettype: '0025',
                ticketno: '0025',
                caseno: '0025',
                patientname: 'VENKATESH BABU',
                salesrep: 'BABU',
                groupname: 'VENKATESH',
                receivedon: '05/29/2020',
                replyon: '05/29/2020',
                status: 'Processing'
            },
            {
                tickettype: '0025',
                ticketno: '0025',
                caseno: '0025',
                patientname: 'VENKATESH BABU',
                salesrep: 'BABU',
                groupname: 'VENKATESH',
                receivedon: '05/29/2020',
                replyon: '05/29/2020',
                status: 'Processing'
            },
        ],
    });

    return (
        <div className={classes.root}>
            <CssBaseline />
            <MaterialTable
                icons={tableIcons}
                title="Case Ticket Report"
                columns={state.columns}
                data={state.data}
                options={{
                    // filtering: true,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    sorting: true,
                    headerStyle: {
                        backgroundColor: '#29b35aab',
                        color: '#fff',
                        padding: '5px 0px',
                        textAlign: 'center'
                    },
                    exportButton: true,
                    exportAllData: true,
                }}
            />
        </div>
    );
};

export default TicketReportLayout;
