import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';

export interface MyCardLayoutProps {

}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
    })
);

const MyCardLayout: React.StatelessComponent<MyCardLayoutProps> = props => {
    const classes = useStyles({});

    return (
        <div className={classes.root}>
            <CssBaseline />
            <h1>MyCard Page</h1>
        </div>
    );
};

export default MyCardLayout;
