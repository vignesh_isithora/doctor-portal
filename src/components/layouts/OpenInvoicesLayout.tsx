import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from 'material-table';
import { Column } from 'material-table/types/index';
import { tableIcons } from './DraftCaseLayout';

interface Row {
    invoiceno: string;
    invoicedate: string;
    lab: string;
    dso: string;
    caseno: string;
    units: string;
    amount: string;
    outstanding: string;
}

interface TableState {
    columns: Array<Column<Row>>;
    data: Row[];
}

export interface OpenInvoicesLayoutProps {

}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
    })
);

const OpenInvoicesLayout: React.StatelessComponent<OpenInvoicesLayoutProps> = props => {
    const classes = useStyles({});
    const [state] = React.useState<TableState>({
        columns: [
            {
                title: 'Invoice No',
                field: 'invoiceno',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Invoice Date',
                field: 'invoicedate',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Lab',
                field: 'lab',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'DSO',
                field: 'dso',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Case No',
                field: 'caseno',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Units',
                field: 'units',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Amount',
                field: 'amount',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Outstanding',
                field: 'outstanding',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
        ],
        data: [
            {
                invoiceno: '0025',
                invoicedate: '05/26/2020',
                lab: 'Lab',
                dso: '05/26/2020',
                caseno: '0025',
                units: '2',
                amount: '20',
                outstanding: 'outstanding',
            },
            {
                invoiceno: '0025',
                invoicedate: '05/26/2020',
                lab: 'Lab',
                dso: '05/26/2020',
                caseno: '0025',
                units: '2',
                amount: '20',
                outstanding: 'outstanding',
            },
            {
                invoiceno: '0025',
                invoicedate: '05/26/2020',
                lab: 'Lab',
                dso: '05/26/2020',
                caseno: '0025',
                units: '2',
                amount: '20',
                outstanding: 'outstanding',
            },
        ],
    });

    return (
        <div className={classes.root}>
            <CssBaseline />
            <MaterialTable
                icons={tableIcons}
                title="Open Invoices"
                columns={state.columns}
                data={state.data}
                options={{
                    // filtering: true,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    sorting: true,
                    headerStyle: {
                        backgroundColor: '#29b35aab',
                        color: '#fff',
                        padding: '5px 0px',
                        textAlign: 'center'
                    },
                    exportButton: true,
                    exportAllData: true,
                }}
            />
        </div>
    );
};

export default OpenInvoicesLayout;
