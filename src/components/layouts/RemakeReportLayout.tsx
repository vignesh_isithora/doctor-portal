import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from 'material-table';
import { Column } from 'material-table/types/index';
import { tableIcons } from './DraftCaseLayout';

interface Row {
    caseno: string;
    patientname: string;
    casesubmission: string;
    shippingdate: string;
    status: string;
    remake: string;
    appointmentdate: string;
}

interface TableState {
    columns: Array<Column<Row>>;
    data: Row[];
}

export interface RemakeReportLayoutProps {

}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
    })
);

const RemakeReportLayout: React.StatelessComponent<RemakeReportLayoutProps> = props => {
    const classes = useStyles({});
    const [state] = React.useState<TableState>({
        columns: [
            {
                title: 'Case No',
                field: 'caseno',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Patient Name',
                field: 'patientname',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Case Submission Date',
                field: 'casesubmission',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Estd.Shipping Date',
                field: 'shippingdate',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Status',
                field: 'status',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Remake Reason',
                field: 'remake',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Appointment Date',
                field: 'appointmentdate',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
        ],
        data: [
            {
                caseno: '0025',
                patientname: 'VENKATESH BABU',
                casesubmission: '05/26/2020',
                shippingdate: '05/26/2020',
                status: 'Processing',
                remake: 'bad',
                appointmentdate: '05/29/2020',
            },
            {
                caseno: '0025',
                patientname: 'VENKATESH BABU',
                casesubmission: '05/26/2020',
                shippingdate: '05/26/2020',
                status: 'Processing',
                remake: 'bad',
                appointmentdate: '05/29/2020',
            },
            {
                caseno: '0025',
                patientname: 'VENKATESH BABU',
                casesubmission: '05/26/2020',
                shippingdate: '05/26/2020',
                status: 'Processing',
                remake: 'bad',
                appointmentdate: '05/29/2020',
            },
        ],
    });

    return (
        <div className={classes.root}>
            <CssBaseline />
            <MaterialTable
                icons={tableIcons}
                title="Case Remake Report"
                columns={state.columns}
                data={state.data}
                options={{
                    // filtering: true,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    sorting: true,
                    headerStyle: {
                        backgroundColor: '#29b35aab',
                        color: '#fff',
                        padding: '5px 0px',
                        textAlign: 'center'
                    },
                    exportButton: true,
                    exportAllData: true,
                }}
            />
        </div>
    );
};

export default RemakeReportLayout;
