import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from 'material-table';
import { Column } from 'material-table/types/index';
import { tableIcons } from './DraftCaseLayout';

interface Row {
  ratedon: string;
  caseno: string;
  patientname: string;
  rating: string;
  feedback: string;
  improvement: string;
}

interface TableState {
  columns: Array<Column<Row>>;
  data: Row[];
}

export interface RatingReportLayoutProps {}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
  })
);

const RatingReportLayout: React.StatelessComponent<RatingReportLayoutProps> = (
  props
) => {
  const classes = useStyles({});
  const [state] = React.useState<TableState>({
    columns: [
      {
        title: 'Rated On',
        field: 'ratedon',
        cellStyle: {
          padding: '10px',
          fontSize: '13px',
          textAlign: 'center',
        },
      },
      {
        title: 'Case No',
        field: 'caseno',
        cellStyle: {
          padding: '10px',
          fontSize: '13px',
          textAlign: 'center',
        },
      },
      {
        title: 'Patient Name',
        field: 'patientname',
        cellStyle: {
          padding: '10px',
          fontSize: '13px',
          textAlign: 'center',
        },
      },
      {
        title: 'Rating',
        field: 'rating',
        cellStyle: {
          padding: '10px',
          fontSize: '13px',
          textAlign: 'center',
        },
      },
      {
        title: 'Feedback/Suggestions/Recommendations',
        field: 'feedback',
        cellStyle: {
          padding: '10px',
          fontSize: '13px',
          textAlign: 'center',
        },
      },
      {
        title: 'Improvements',
        field: 'improvement',
        cellStyle: {
          padding: '10px',
          fontSize: '13px',
          textAlign: 'center',
        },
      },
      {
        title: 'Status',
        field: 'status',
        cellStyle: {
          padding: '10px',
          fontSize: '13px',
          textAlign: 'center',
        },
      },
    ],
    data: [
      {
        ratedon: '05/26/2020',
        caseno: '0025',
        patientname: 'VENKATESH BABU',
        rating: '4',
        feedback: 'good',
        improvement: 'improvement',
      },
      {
        ratedon: '05/26/2020',
        caseno: '0025',
        patientname: 'VENKATESH BABU',
        rating: '7',
        feedback: 'bad',
        improvement: 'improvement',
      },
      {
        ratedon: '05/26/2020',
        caseno: '0025',
        patientname: 'VENKATESH BABU',
        rating: '6',
        feedback: 'not bad',
        improvement: 'improvement',
      },
    ],
  });

  return (
    <div className={classes.root}>
      <CssBaseline />
      <MaterialTable
        icons={tableIcons}
        title="Case Rating Report"
        columns={state.columns}
        data={state.data}
        options={{
          // filtering: true,
          pageSizeOptions: [5, 10, 25, 50, 100],
          sorting: true,
          headerStyle: {
            backgroundColor: '#29b35aab',
            color: '#fff',
            padding: '5px 0px',
            textAlign: 'center',
          },
          exportButton: true,
          exportAllData: true,
        }}
      />
    </div>
  );
};

export default RatingReportLayout;
