import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from 'material-table';
import { Column } from 'material-table/types/index';
import { tableIcons } from './DraftCaseLayout';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';

interface Row {
    bookingdate: string;
    trackingno: string;
    weight: string;
    caseno: string;
    pickupaddress: string;
}

interface TableState {
    columns: Array<Column<Row>>;
    data: Row[];
}

export interface PrintUpsLabelLayoutProps {

}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
    })
);

const PrintUpsLabelLayout: React.StatelessComponent<PrintUpsLabelLayoutProps> = props => {
    const classes = useStyles({});
    const [state] = React.useState<TableState>({
        columns: [
            {
                title: 'Booking Date',
                field: 'bookingdate',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Tracking No',
                field: 'trackingno',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Weight',
                field: 'weight',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Case No',
                field: 'caseno',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Pickup Address',
                field: 'pickupaddress',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Details',
                field: 'fdownload',
                export: false,
                // cellStyle: {
                //   backgroundColor: '#039be5',
                //   color: '#FFF'
                // },
                render: rowData => (
                    // <div style={{ display: 'flex' }}>
                    <Tooltip title="View" placement="top">
                        <IconButton aria-label="view" >
                            <VisibilityIcon fontSize="small" />
                        </IconButton>
                    </Tooltip>
                    // </div>
                ),
                cellStyle: {
                    padding: '0px',
                    textAlign: 'center'
                }
            }
        ],
        data: [
            {
                bookingdate: '05/26/2020',
                trackingno: '1Z3487870198308598',
                weight: '2',
                caseno: 'CN351998',
                pickupaddress: 'Main',
            },
            {
                bookingdate: '05/26/2020',
                trackingno: '1Z3487870198308598',
                weight: '2',
                caseno: 'CN351998',
                pickupaddress: 'Main',
            },
            {
                bookingdate: '05/26/2020',
                trackingno: '1Z3487870198308598',
                weight: '2',
                caseno: 'CN351998',
                pickupaddress: 'Main',
            },
        ],
    });

    return (
        <div className={classes.root}>
            <CssBaseline />
            <MaterialTable
                icons={tableIcons}
                title="Print UPS Labels"
                columns={state.columns}
                data={state.data}
                options={{
                    // filtering: true,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    sorting: true,
                    headerStyle: {
                        backgroundColor: '#29b35aab',
                        color: '#fff',
                        padding: '5px 0px',
                        textAlign: 'center'
                    },
                    exportButton: true,
                    exportAllData: true,
                }}
            />
        </div>
    );
};

export default PrintUpsLabelLayout;
