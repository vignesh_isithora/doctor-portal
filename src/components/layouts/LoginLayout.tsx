import * as React from 'react';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import HeaderContainer from '../Header/HeaderContainer';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import LocalOfferRoundedIcon from '@material-ui/icons/LocalOfferRounded';
import BookRoundedIcon from '@material-ui/icons/BookRounded';
import HelpRoundedIcon from '@material-ui/icons/HelpRounded';
import UpdateRoundedIcon from '@material-ui/icons/UpdateRounded';
import Hidden from '@material-ui/core/Hidden';

export interface LoginLayoutProps {
    onSubmit: (username: string, password: string) => void;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            height: '100vh'
        },
        main: {
            flexGrow: 1,
            height: '92vh'
        },
        image: {
            backgroundImage: 'linear-gradient(rgba(0,0,0,.6), rgba(0,0,0,.6)),url(/assets/dental.jpg)',
            backgroundRepeat: 'no-repeat',
            backgroundColor:
                theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-end'
        },
        font: {
            color: '#fff',
            fontSize: '50px'
        },
        paragraph: {
            color: '#fff',
            fontSize: '20px'
        },
        paper: {
            margin: theme.spacing(0, 4),
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%'
        },
        paper2: {
            margin: 'auto 32px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            // height: '70%',
            width: '60%'
        },
        paper3: {
            // margin: theme.spacing(2, 4),
            display: 'flex',
            // flexDirection: 'column',
            justifyContent: 'center',
            // height: '25%',
            width: '100%',
            background: 'linear-gradient(to bottom,rgba(0, 0, 0, 0),rgb(0, 0, 0))'
        },
        card: {
            margin: '25px',
            textAlign: 'center',
            background: 'transparent'
        },
        typographyCard: {
            color: '#FFF',
            textAlign: 'center',
            fontSize: '14px'
        },
        avatar: {
            margin: theme.spacing(1),
            backgroundColor: theme.palette.secondary.main,
        },
        form: {
            width: '100%', // Fix IE 11 issue.
            marginTop: theme.spacing(1),
        },
        formControl: {
            // margin: theme.spacing(1),
            marginTop: theme.spacing(1),
            width: '100%',
        },
        submit: {
            margin: theme.spacing(3, 0, 2),
        },
        links: {
            display: 'flex',
            justifyContent: 'space-between'
        }
    })
);

const LoginLayout: React.StatelessComponent<LoginLayoutProps> = props => {
    const classes = useStyles({});
    const [userName, setuserName] = React.useState('');
    const [passWord, setPassword] = React.useState('');
    const saveUsername = (username: string) => {
        setuserName(username);
    };
    const savePassword = (password: string) => {
        setPassword(password);
    };
    const submit = () => {
        props.onSubmit(userName, passWord);
        setuserName('');
        setPassword('');
    };

    return (
        <div className={classes.root}>
            <CssBaseline />
            <HeaderContainer />
            <Grid container component="main" className={classes.main}>
                <Hidden xsDown>
                    <Grid item xs={false} sm={7} md={9} className={classes.image}>
                        <div className={classes.paper2}>
                            <Typography variant="h2" component="h4" className={classes.font}>
                                A digital platform that connects dentists to dental labs in just few clicks
                            </Typography>
                        </div>
                        <div className={classes.paper3}>
                            <Paper elevation={0} className={classes.card}>
                                <BookRoundedIcon style={{ fontSize: 50, color: '#FFF' }} />
                                <Typography variant="h6" gutterBottom style={{ color: '#FFF' }}>
                                    Book Cases On The Go
                                </Typography>
                                <Hidden smDown>
                                    <Typography component="div" className={classes.typographyCard}>
                                        A simpler way to submit
                                        your cases with any
                                        device no matter where you
                                        are!
                                    </Typography>
                                </Hidden>
                            </Paper>
                            <Paper elevation={0} className={classes.card}>
                                <HelpRoundedIcon style={{ fontSize: 50, color: '#FFF' }} />
                                <Typography variant="h6" gutterBottom style={{ color: '#FFF' }}>
                                    One Click Support
                                </Typography>
                                <Hidden smDown>
                                    <Typography component="div" className={classes.typographyCard}>
                                        Getting assistance for any
                                        of your inquiries is a lot
                                        easier with MyLabConnect.
                                    </Typography>
                                </Hidden>
                            </Paper>
                            <Paper elevation={0} className={classes.card}>
                                <UpdateRoundedIcon style={{ fontSize: 50, color: '#FFF' }} />
                                <Typography variant="h6" gutterBottom style={{ color: '#FFF' }}>
                                    Real-time Tracking
                                </Typography>
                                <Hidden smDown>
                                    <Typography component="div" className={classes.typographyCard}>
                                        Get live updates of your
                                        Shipment and save your
                                        time!
                                    </Typography>
                                </Hidden>
                            </Paper>
                            <Paper elevation={0} className={classes.card}>
                                <LocalOfferRoundedIcon style={{ fontSize: 50, color: '#FFF' }} />
                                <Typography variant="h6" gutterBottom style={{ color: '#FFF' }}>
                                    Exclusive Offers
                                </Typography>
                                <Hidden smDown>
                                    <Typography component="div" className={classes.typographyCard}>
                                        Stay up to date on special
                                        promotions and discounts
                                        offered
                                    </Typography>
                                </Hidden>
                            </Paper>
                        </div>
                    </Grid>
                </Hidden>
                <Grid item xs={12} sm={5} md={3} component={Paper} square>
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Login In
                        </Typography>
                        <form className={classes.form} noValidate>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Username"
                                name="email"
                                autoComplete="email"
                                autoFocus
                                onChange={(event) => saveUsername(event.target.value as string)}
                                value={userName}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                onChange={(event) => savePassword(event.target.value as string)}
                                onKeyPress={event => {
                                    if (event.key === 'Enter') {
                                        submit();
                                    }
                                }
                                }
                                value={passWord}
                            />
                            <FormControlLabel
                                control={<Checkbox value="remember" color="primary" />}
                                label="Remember me"
                            />
                            <Button
                                type="button"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                onClick={() => submit()}
                            >
                                Login In
                            </Button>
                            <div className={classes.links}>
                                <Grid item xs>
                                    <Link href="#" variant="body2">
                                        Forgot password?
                                    </Link>
                                </Grid>
                                <Hidden smUp>
                                    <Grid item>
                                        <Link href="#" variant="body2">
                                            Sign Up
                                        </Link>
                                    </Grid>
                                </Hidden>
                            </div>
                        </form>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
};

export default LoginLayout;
