import * as React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

export interface ErrorProps {

}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            // height: 'calc(100vh - 13px)'
            backgroundColor: '#50D6D9'
        },
        innerText: {
            backgroundColor: '#50D6D9',
            // height: '100vh'
        },
        center: {
            height: '100vh',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column'
        },
        image: {
            maxHeight: '40vh'
        },
        text: {
            fontWeight: 300,
            color: '#fff',
            textAlign: 'center'
        },
        paragraph: {
            color: '#fff',
            textAlign: 'center'
        }
    }),
);

const ErrorPageView: React.StatelessComponent<ErrorProps> = props => {
    const classes = useStyles({});
    return (
        <div className={classes.root}>
            <CssBaseline />
            <Container fixed>
                <div className={classes.innerText}>
                    <div className={classes.center}>
                        <img src="/assets/error.jpg" alt="error page" className={classes.image} />
                        <Typography variant="h3" gutterBottom className={classes.text}>
                            Whoops! Page not found
                        </Typography>
                        <Typography variant="body2" gutterBottom className={classes.paragraph}>
                            We are very sorry for the inconvenience. <br />It looks like you're trying to access a page that has been deleted or never even existed.
                        </Typography>
                    </div>
                </div>
            </Container>
        </div>
    );
};

export default ErrorPageView;
