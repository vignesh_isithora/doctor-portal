import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';

export interface TrackPickupLayoutProps {

}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
    })
);

const TrackPickupLayout: React.StatelessComponent<TrackPickupLayoutProps> = props => {
    const classes = useStyles({});

    return (
        <div className={classes.root}>
            <CssBaseline />
            <h1>TrackPickup Page</h1>
        </div>
    );
};

export default TrackPickupLayout;
