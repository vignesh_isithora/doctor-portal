import * as React from 'react';
import createStyles from '@material-ui/core/styles/createStyles';
import makeStyles from '@material-ui/core/styles/makeStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import HttpsRoundedIcon from '@material-ui/icons/HttpsRounded';
import MailRoundedIcon from '@material-ui/icons/MailRounded';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import Hidden from '@material-ui/core/Hidden';

export interface HeaderLayoutProps {

}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      height: '8vh'
    },
    colorHeader: {
      backgroundColor: '#FFF',
      color: '#000000b3'
    },
    title: {
      flexGrow: 1
    },
    logo: {
      // padding: '6px 0px',
      height: '6vh',
      width: 'auto'
    },
    link: {
      textDecoration: 'none',
      color: 'currentColor'
    },
    sectionDesktop: {
      display: 'flex',
      alignItems: 'center',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
        alignItems: 'center',
      },
    },
  })
);

const IconButtons = withStyles({
  root: {
    '& .MuiButtonBase-root': {
      cursor: 'none !importent'
    },
    '& .MuiIconButton-root': {
      '&:hover': {
        backgroundColor: 'none !importent',
      },
    }
  }
})(IconButton);

const HeaderLayout: React.StatelessComponent<HeaderLayoutProps> = props => {
  const classes = useStyles({});

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.colorHeader}>
        <Toolbar>
          <img src="/assets/logo-login.png" title="MyLabConnect" className={classes.logo} />
          <Typography variant="h6" className={classes.title}>
            &nbsp;
          </Typography>
          <div className={classes.sectionDesktop}>
            <IconButtons
              aria-label="show 4 new mails"
              color="inherit"
              disabled
            />
            <HttpsRoundedIcon style={{ fontSize: 25, color: '#000000b3' }} />
            <Tooltip title="Sign In" aria-label="add">
              <Link to={'/login'} className={classes.link}>
                <Button color="inherit">Sign In</Button>
              </Link>
            </Tooltip>
            <Hidden xsDown>
              <IconButtons
                aria-label="show 4 new mails"
                color="inherit"
                disabled
              />
              <PersonAddIcon style={{ fontSize: 25, color: '#000000b3' }} />
              <Tooltip title="Sign Up" aria-label="add">
                <Link to={'/signup'} className={classes.link}>
                  <Button color="inherit">Sign Up</Button>
                </Link>
              </Tooltip>
              <IconButtons
                aria-label="show 4 new mails"
                color="inherit"
                disabled
              />
              <MailRoundedIcon style={{ fontSize: 25, color: '#000000b3' }} />
              <Tooltip title="Contact" aria-label="add">
                <Link to={'/contact'} className={classes.link}>
                  <Button color="inherit">Contact Us</Button>
                </Link>
              </Tooltip>
            </Hidden>
          </div>
        </Toolbar>
      </AppBar>
    </div >
  );
};

export default HeaderLayout;
