import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from 'material-table';
import { Column } from 'material-table/types/index';
import { tableIcons } from './DraftCaseLayout';

interface Row {
    caseno: string;
    doctercode: string;
    paydate: string;
    invoiceno: string;
    amount: string;
    refno: string;
}

interface TableState {
    columns: Array<Column<Row>>;
    data: Row[];
}

export interface PaymentRegisterLayoutProps {

}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
    })
);

const PaymentRegisterLayout: React.StatelessComponent<PaymentRegisterLayoutProps> = props => {
    const classes = useStyles({});
    const [state] = React.useState<TableState>({
        columns: [
            {
                title: 'Case No',
                field: 'caseno',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Docter Code',
                field: 'doctercode',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Payment Date',
                field: 'paydate',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Invoice No',
                field: 'invoiceno',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Amount',
                field: 'amount',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Reference No',
                field: 'refno',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
        ],
        data: [
            {
                caseno: '0025',
                doctercode: 'doctercode',
                paydate: '05/26/2020',
                invoiceno: '0025',
                amount: '100',
                refno: '12345'
            },
            {
                caseno: '0025',
                doctercode: 'doctercode',
                paydate: '05/26/2020',
                invoiceno: '0025',
                amount: '200',
                refno: '5632'
            },
            {
                caseno: '0025',
                doctercode: 'doctercode',
                paydate: '05/26/2020',
                invoiceno: '0025',
                amount: '300',
                refno: '98745'
            },
        ],
    });

    return (
        <div className={classes.root}>
            <CssBaseline />
            <MaterialTable
                icons={tableIcons}
                title="Payment Register"
                columns={state.columns}
                data={state.data}
                options={{
                    // filtering: true,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    sorting: true,
                    headerStyle: {
                        backgroundColor: '#29b35aab',
                        color: '#fff',
                        padding: '5px 0px',
                        textAlign: 'center'
                    },
                    exportButton: true,
                    exportAllData: true,
                }}
            />
        </div>
    );
};

export default PaymentRegisterLayout;
