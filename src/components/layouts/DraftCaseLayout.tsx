import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from 'material-table';
import { Column } from 'material-table/types/index';
import { forwardRef } from 'react';
import { Icons } from 'material-table/types/index';
import DescriptionSharpIcon from '@material-ui/icons/DescriptionSharp';

export const tableIcons: Icons = {
    Export: forwardRef((props, ref) => <DescriptionSharpIcon {...props} ref={ref} />)
};

interface Row {
    patientname: string;
    createdon: string;
    gender: string;
    age: string;
    shippingdate: string;
    appointmentdate: string;
}

interface TableState {
    columns: Array<Column<Row>>;
    data: Row[];
}

export interface DraftCaseLayoutProps {

}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
    })
);

const DraftCaseLayout: React.StatelessComponent<DraftCaseLayoutProps> = props => {
    const classes = useStyles({});
    const [state] = React.useState<TableState>({
        columns: [
            {
                title: 'Patient Name',
                field: 'patientname',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Created On',
                field: 'createdon',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Gender',
                field: 'gender',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Age(Years)',
                field: 'age',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Estd.Shipping Date',
                field: 'shippingdate',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Appointment Date',
                field: 'appointmentdate',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
        ],
        data: [
            {
                patientname: 'VENKATESH BABU',
                createdon: '05/26/2020',
                age: '25',
                gender: 'Male',
                shippingdate: '05/26/2020',
                appointmentdate: '05/29/2020'
            },
            {
                patientname: 'VENKATESH BABU',
                createdon: '05/26/2020',
                age: '25',
                gender: 'Male',
                shippingdate: '05/26/2020',
                appointmentdate: '05/29/2020'
            },
            {
                patientname: 'VENKATESH BABU',
                createdon: '05/26/2020',
                age: '25',
                gender: 'Male',
                shippingdate: '05/26/2020',
                appointmentdate: '05/29/2020'
            },
        ],
    });

    return (
        <div className={classes.root}>
            <CssBaseline />
            <MaterialTable
                icons={tableIcons}
                title="Draft Cases"
                columns={state.columns}
                data={state.data}
                options={{
                    // filtering: true,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    sorting: true,
                    headerStyle: {
                        backgroundColor: '#29b35aab',
                        color: '#fff',
                        padding: '5px 0px',
                        textAlign: 'center'
                    },
                    exportButton: true,
                    exportAllData: true,
                }}
            />
        </div>
    );
};

export default DraftCaseLayout;
