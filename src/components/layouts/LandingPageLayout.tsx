import * as React from 'react';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import createStyles from '@material-ui/core/styles/createStyles';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import CssBaseline from '@material-ui/core/CssBaseline';
import clsx from 'clsx';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import Tooltip from '@material-ui/core/Tooltip';
import HomeRoundedIcon from '@material-ui/icons/HomeRounded';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MoreIcon from '@material-ui/icons/MoreVert';
import useTheme from '@material-ui/core/styles/useTheme';
import withStyles from '@material-ui/core/styles/withStyles';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
// import Popover from '@material-ui/core/Popover';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import DraftsRoundedIcon from '@material-ui/icons/DraftsRounded';
import LiveTvRoundedIcon from '@material-ui/icons/LiveTvRounded';
import LocalShippingRoundedIcon from '@material-ui/icons/LocalShippingRounded';
import MoodRoundedIcon from '@material-ui/icons/MoodRounded';
import PlaylistAddCheckRoundedIcon from '@material-ui/icons/PlaylistAddCheckRounded';
import AssignmentReturnedRoundedIcon from '@material-ui/icons/AssignmentReturnedRounded';
import ScheduleRoundedIcon from '@material-ui/icons/ScheduleRounded';
import FingerprintRoundedIcon from '@material-ui/icons/FingerprintRounded';
import TrackChangesRoundedIcon from '@material-ui/icons/TrackChangesRounded';
import PrintRoundedIcon from '@material-ui/icons/PrintRounded';
import SpeakerNotesRoundedIcon from '@material-ui/icons/SpeakerNotesRounded';
import StarHalfRoundedIcon from '@material-ui/icons/StarHalfRounded';
import CategoryRoundedIcon from '@material-ui/icons/CategoryRounded';
import ThumbsUpDownRoundedIcon from '@material-ui/icons/ThumbsUpDownRounded';
import AnnouncementRoundedIcon from '@material-ui/icons/AnnouncementRounded';
import EmojiPeopleRoundedIcon from '@material-ui/icons/EmojiPeopleRounded';
import PaymentRoundedIcon from '@material-ui/icons/PaymentRounded';
import ReceiptRoundedIcon from '@material-ui/icons/ReceiptRounded';
import AccountBalanceWalletRoundedIcon from '@material-ui/icons/AccountBalanceWalletRounded';
import CreateRoundedIcon from '@material-ui/icons/CreateRounded';
import CreditCardRoundedIcon from '@material-ui/icons/CreditCardRounded';
import LocalPrintshopRoundedIcon from '@material-ui/icons/LocalPrintshopRounded';
import DialpadRoundedIcon from '@material-ui/icons/DialpadRounded';
import ViewListRoundedIcon from '@material-ui/icons/ViewListRounded';
import ReportRoundedIcon from '@material-ui/icons/ReportRounded';
import RedoRoundedIcon from '@material-ui/icons/RedoRounded';
import StarsRoundedIcon from '@material-ui/icons/StarsRounded';
import AccountBalanceRoundedIcon from '@material-ui/icons/AccountBalanceRounded';
import LocalActivityRoundedIcon from '@material-ui/icons/LocalActivityRounded';
import BallotRoundedIcon from '@material-ui/icons/BallotRounded';
import { Route } from 'react-router-dom';
import { Redirect, Switch } from 'react-router';

import DashboardContainer from '../Dashboard/DashboardContainer';
import AddNewCaseContainer from '../AddNewCases/AddNewCaseContainer';
import DraftCaseContainer from '../DraftCase/DraftCaseContainer';
import LiveCaseContainer from '../LiveCase/LiveCaseContainer';
import ShippingCaseContainer from '../ShippingCase/ShippingCaseContainer';
import SmileCaseContainer from '../SmileCase/SmileCaseContainer';
import GetLabelContainer from '../GetLabel/GetLabelContainer';
import SchedulePickupContainer from '../SchedulePickup/SchedulePickupContainer';
import ScanLabelContainer from '../ScanLabel/ScanLabelContainer';
import TrackPickupContainer from '../TrackPickup/TrackPickupContainer';
import PrintUpsLabelContainer from '../PrintUpsLabel/PrintUpsLabelContainer';
import RatingContainer from '../Rating/RatingContainer';
import OrderSuppliesContainer from '../OrderSupplies/OrderSuppliesContainer';
import SupportContainer from '../Support/SupportContainer';
import EventsContainer from '../Events/EventsContainer';
import PromotionsContainer from '../Promotions/PromotionsContainer';
import PaymentContainer from '../Payment/PaymentContainer';
import OpenInvoicesContainer from '../OpenInvoices/OpenInvoicesContainer';
import DraftPaymentContainer from '../DraftPayment/DraftPaymentContainer';
import PaymentRegisterContainer from '../PaymentRegister/PaymentRegisterContainer';
import MyCardContainer from '../MyCard/MyCardContainer';
import RxFormContainer from '../RxForm/RxFormContainer';
import RestorationContainer from '../Restoration/RestorationContainer';
import ListReportContainer from '../ListReport/ListReportContainer';
import RemakeReportContainer from '../RemakeReport/RemakeReportContainer';
import RatingReportContainer from '../RatingReport/RatingReportContainer';
import TicketReportContainer from '../TicketReport/TicketReportContainer';

export interface LandingPageProps {
    navTo: (nav: string) => void;
}
const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            height: '100vh'
        },
        appBar: {
            transition: theme.transitions.create(['margin', 'width'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
            backgroundColor: '#20AAE1',
            color: '#FFF'
        },
        appBarShift: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
            transition: theme.transitions.create(['margin', 'width'], {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        hide: {
            display: 'none',
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        drawerPaper: {
            width: drawerWidth,
        },
        drawerHeader: {
            display: 'flex',
            alignItems: 'center',
            padding: theme.spacing(0, 1),
            ...theme.mixins.toolbar,
            justifyContent: 'center',
        },
        search: {
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: fade(theme.palette.common.white, 0.25),
            },
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(1),
                width: 'auto',
            },
        },
        searchIcon: {
            padding: theme.spacing(0, 2),
            height: '100%',
            position: 'absolute',
            pointerEvents: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        inputRoot: {
            color: 'inherit',
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                width: '12ch',
                '&:focus': {
                    width: '20ch',
                },
            },
        },
        grow: {
            flexGrow: 1,
        },
        sectionDesktop: {
            display: 'none',
            [theme.breakpoints.up('md')]: {
                display: 'flex',
            },
        },
        sectionMobile: {
            display: 'flex',
            [theme.breakpoints.up('md')]: {
                display: 'none',
            },
        },
        logo: {
            height: '50px',
            width: 'auto',
            cursor: 'pointer'
        },
        list: {
            paddingTop: '4px',
            paddingBottom: '4px'
        },
        nested: {
            paddingLeft: theme.spacing(4),
        },
        collapse: {
            minHeight: 'auto !important'
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
            marginLeft: -drawerWidth,
        },
        contentShift: {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        },
    })
);

const ListItemStyle = withStyles({
    root: {
        paddingTop: '4px',
        paddingBottom: '4px'
    },
})(ListItem);

export const LandingPageLayout: React.StatelessComponent<LandingPageProps> = (props) => {
    const classes = useStyles({});
    const theme = useTheme();
    const [open, setOpen] = React.useState(true);
    const [myCaseOpen, setmyCaseOpen] = React.useState(true);
    const [pickupOpen, setpickupOpen] = React.useState(false);
    const [communicationOpen, setcommunicationOpen] = React.useState(false);
    const [paymentOpen, setpaymentOpen] = React.useState(false);
    const [utilsOpen, setutilsOpen] = React.useState(false);
    const [reportOpen, setreportOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const myCaseNav = () => {
        setmyCaseOpen(!myCaseOpen);
        setpickupOpen(false);
        setcommunicationOpen(false);
        setpaymentOpen(false);
        setutilsOpen(false);
        setreportOpen(false);
    };
    const pickupNav = () => {
        setmyCaseOpen(false);
        setpickupOpen(!pickupOpen);
        setcommunicationOpen(false);
        setpaymentOpen(false);
        setutilsOpen(false);
        setreportOpen(false);
    };
    const communicationNav = () => {
        setmyCaseOpen(false);
        setpickupOpen(false);
        setcommunicationOpen(!communicationOpen);
        setpaymentOpen(false);
        setutilsOpen(false);
        setreportOpen(false);
    };
    const paymentNav = () => {
        setmyCaseOpen(false);
        setpickupOpen(false);
        setcommunicationOpen(false);
        setpaymentOpen(!paymentOpen);
        setutilsOpen(false);
        setreportOpen(false);
    };
    const utilsNav = () => {
        setmyCaseOpen(false);
        setpickupOpen(false);
        setcommunicationOpen(false);
        setpaymentOpen(false);
        setutilsOpen(!utilsOpen);
        setreportOpen(false);
    };
    const reportNav = () => {
        setmyCaseOpen(false);
        setpickupOpen(false);
        setcommunicationOpen(false);
        setpaymentOpen(false);
        setutilsOpen(false);
        setreportOpen(!reportOpen);
    };

    const menuId = 'primary-search-account-menu';
    const mobileMenuId = 'primary-search-account-menu-mobile';

    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState<null | HTMLElement>(null);

    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const handleMobileMenuClose = () => {
        setMobileMoreAnchorEl(null);
    };

    const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
        setMobileMoreAnchorEl(event.currentTarget);
    };

    const renderMobileMenu = (
        <Menu
            anchorEl={mobileMoreAnchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={mobileMenuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMobileMenuOpen}
            onClose={handleMobileMenuClose}
        >
            <MenuItem>
                <IconButton aria-label="show 4 new mails" color="inherit" onClick={() => props.navTo('home')}>
                    <HomeRoundedIcon />
                </IconButton>
            </MenuItem>
            <MenuItem>
                <IconButton aria-label="show 17 new notifications" color="inherit">
                    <Badge badgeContent={4} color="secondary">
                        <NotificationsIcon />
                    </Badge>
                </IconButton>
            </MenuItem>
            <MenuItem>
                <IconButton
                    aria-label="account of current user"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    // onClick={props.profilePage}
                    color="inherit"
                >
                    <AccountCircle />
                </IconButton>
            </MenuItem>
            <MenuItem>
                <IconButton
                    edge="end"
                    aria-label="account of current user"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={() => props.navTo('login')}
                    color="inherit"
                >
                    <ExitToAppIcon />
                </IconButton>
            </MenuItem>
        </Menu>
    );
    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, open && classes.hide)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Link
                        component="button"
                        className={clsx(classes.menuButton, open && classes.hide)}
                    >
                        <Typography style={{ color: 'white' }} variant="h6" noWrap>MyLab Connect</Typography>
                    </Link>
                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon />
                        </div>
                        <InputBase
                            placeholder="Search…"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </div>
                    <div className={classes.grow} />
                    <div className={classes.sectionDesktop}>
                        <IconButton aria-label="show 4 new mails" color="inherit" onClick={() => props.navTo('home')}>
                            <Tooltip title="Home" aria-label="add">
                                <HomeRoundedIcon />
                            </Tooltip>
                        </IconButton>
                        <IconButton aria-label="show 17 new notifications" color="inherit">
                            <Badge badgeContent={4} color="secondary">
                                <Tooltip title="Notification" aria-label="add">
                                    <NotificationsIcon />
                                </Tooltip>
                            </Badge>
                        </IconButton>
                        <IconButton
                            aria-label="account of current user"
                            aria-controls={menuId}
                            aria-haspopup="true"
                            // onClick={props.profilePage}
                            color="inherit"
                        >
                            <Tooltip title="Profile" aria-label="add">
                                <AccountCircle />
                            </Tooltip>
                        </IconButton>
                        <IconButton
                            edge="end"
                            aria-label="account of current user"
                            aria-controls={menuId}
                            aria-haspopup="true"
                            onClick={() => props.navTo('login')}
                            color="inherit"
                        >
                            <Tooltip title="Logout" aria-label="add">
                                <ExitToAppIcon />
                            </Tooltip>
                        </IconButton>
                    </div>
                    <div className={classes.sectionMobile}>
                        <IconButton
                            aria-label="show more"
                            aria-controls={mobileMenuId}
                            aria-haspopup="true"
                            onClick={handleMobileMenuOpen}
                            color="inherit"
                        >
                            <MoreIcon />
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
            {renderMobileMenu}
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerHeader}>
                    <img src="/assets/logo-login.png" title="EasyDent" onClick={() => props.navTo('home')} className={classes.logo} />
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </div>
                <Divider />
                <List className={classes.list}>
                    <ListItemStyle button onClick={() => props.navTo('home')} key="Home">
                        <ListItemIcon>
                            <HomeRoundedIcon />
                        </ListItemIcon>
                        <ListItemText primary="Home" />
                    </ListItemStyle>
                </List>
                <List className={classes.list}>
                    <ListItemStyle button onClick={() => props.navTo('newCase')} key="Add New Cases">
                        <ListItemIcon>
                            <CloudUploadIcon />
                        </ListItemIcon>
                        <ListItemText primary="Add New Cases" />
                    </ListItemStyle>
                </List>
                <ListItemStyle button onClick={myCaseNav}>
                    <ListItemIcon>
                        <FormatListBulletedIcon />
                    </ListItemIcon>
                    <ListItemText primary="My Cases" />
                    {myCaseOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItemStyle>
                <Collapse in={myCaseOpen} timeout="auto" unmountOnExit className={classes.collapse}>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('draft')} className={classes.nested}>
                            <ListItemIcon>
                                <DraftsRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Draft Cases" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('live')} className={classes.nested}>
                            <ListItemIcon>
                                <LiveTvRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Live Cases" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('ship')} className={classes.nested}>
                            <ListItemIcon>
                                <LocalShippingRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Shipped Cases" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('smile')} className={classes.nested}>
                            <ListItemIcon>
                                <MoodRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Smile Cases" />
                        </ListItemStyle>
                    </List>
                </Collapse>
                <ListItemStyle button onClick={pickupNav}>
                    <ListItemIcon>
                        <PlaylistAddCheckRoundedIcon />
                    </ListItemIcon>
                    <ListItemText primary="Pickup" />
                    {pickupOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItemStyle>
                <Collapse in={pickupOpen} timeout="auto" unmountOnExit className={classes.collapse}>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('getLabel')} className={classes.nested}>
                            <ListItemIcon>
                                <AssignmentReturnedRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Get Label" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('schedule')} className={classes.nested}>
                            <ListItemIcon>
                                <ScheduleRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Schedule Pickup" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('scan')} className={classes.nested}>
                            <ListItemIcon>
                                <FingerprintRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Scan Label" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('track')} className={classes.nested}>
                            <ListItemIcon>
                                <TrackChangesRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Track Pickup" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('ups')} className={classes.nested}>
                            <ListItemIcon>
                                <PrintRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Print UPS Labels" />
                        </ListItemStyle>
                    </List>
                </Collapse>
                <ListItemStyle button onClick={communicationNav}>
                    <ListItemIcon>
                        <SpeakerNotesRoundedIcon />
                    </ListItemIcon>
                    <ListItemText primary="Communication" />
                    {communicationOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItemStyle>
                <Collapse in={communicationOpen} timeout="auto" unmountOnExit className={classes.collapse}>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('rating')} className={classes.nested}>
                            <ListItemIcon>
                                <StarHalfRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Rating" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('supplies')} className={classes.nested}>
                            <ListItemIcon>
                                <CategoryRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Order Supplies" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('support')} className={classes.nested}>
                            <ListItemIcon>
                                <ThumbsUpDownRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Support" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('news')} className={classes.nested}>
                            <ListItemIcon>
                                <AnnouncementRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="News/Events" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('promotions')} className={classes.nested}>
                            <ListItemIcon>
                                <EmojiPeopleRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Promotions" />
                        </ListItemStyle>
                    </List>
                </Collapse>
                <ListItemStyle button onClick={paymentNav}>
                    <ListItemIcon>
                        <AccountBalanceRoundedIcon />
                    </ListItemIcon>
                    <ListItemText primary="Payments" />
                    {paymentOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItemStyle>
                <Collapse in={paymentOpen} timeout="auto" unmountOnExit className={classes.collapse}>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('payment')} className={classes.nested}>
                            <ListItemIcon>
                                <PaymentRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Payment" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('invoices')} className={classes.nested}>
                            <ListItemIcon>
                                <ReceiptRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Open Invoices" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('draftPayment')} className={classes.nested}>
                            <ListItemIcon>
                                <AccountBalanceWalletRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Draft Payment" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('register')} className={classes.nested}>
                            <ListItemIcon>
                                <CreateRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Payment Register" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('myCards')} className={classes.nested}>
                            <ListItemIcon>
                                <CreditCardRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="My Cards" />
                        </ListItemStyle>
                    </List>
                </Collapse>
                <ListItemStyle button onClick={utilsNav}>
                    <ListItemIcon>
                        <BallotRoundedIcon />
                    </ListItemIcon>
                    <ListItemText primary="Utils" />
                    {utilsOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItemStyle>
                <Collapse in={utilsOpen} timeout="auto" unmountOnExit className={classes.collapse}>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('rxForm')} className={classes.nested}>
                            <ListItemIcon>
                                <LocalPrintshopRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Print RX Form" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('estimation')} className={classes.nested}>
                            <ListItemIcon>
                                <DialpadRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Restoration Estimation Calculator" />
                        </ListItemStyle>
                    </List>
                </Collapse>
                <ListItemStyle button onClick={reportNav}>
                    <ListItemIcon>
                        <ViewListRoundedIcon />
                    </ListItemIcon>
                    <ListItemText primary="Reports" />
                    {reportOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItemStyle>
                <Collapse in={reportOpen} timeout="auto" unmountOnExit className={classes.collapse}>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('listReport')} className={classes.nested}>
                            <ListItemIcon>
                                <ReportRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Case List Report" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('remakeReport')} className={classes.nested}>
                            <ListItemIcon>
                                <RedoRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Case Remake Report" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('ratingReport')} className={classes.nested}>
                            <ListItemIcon>
                                <StarsRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Case Rating Report" />
                        </ListItemStyle>
                    </List>
                    <List component="div" disablePadding>
                        <ListItemStyle button onClick={() => props.navTo('ticketReport')} className={classes.nested}>
                            <ListItemIcon>
                                <LocalActivityRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Case Ticket Report" />
                        </ListItemStyle>
                    </List>
                </Collapse>
            </Drawer>
            <main
                className={clsx(useStyles({}).content, {
                    [useStyles({}).contentShift]: open,
                })}
            >
                <div className={classes.drawerHeader} />
                <Switch>
                    <Route exact path="/app/home" component={DashboardContainer} />
                    <Route path="/app/newCase" component={AddNewCaseContainer} />
                    <Route path="/app/draft" component={DraftCaseContainer} />
                    <Route path="/app/live" component={LiveCaseContainer} />
                    <Route path="/app/shipping" component={ShippingCaseContainer} />
                    <Route path="/app/smile" component={SmileCaseContainer} />
                    <Route path="/app/getLabel" component={GetLabelContainer} />
                    <Route path="/app/schedule" component={SchedulePickupContainer} />
                    <Route path="/app/scan" component={ScanLabelContainer} />
                    <Route path="/app/track" component={TrackPickupContainer} />
                    <Route path="/app/ups" component={PrintUpsLabelContainer} />
                    <Route path="/app/rating" component={RatingContainer} />
                    <Route path="/app/supplies" component={OrderSuppliesContainer} />
                    <Route path="/app/support" component={SupportContainer} />
                    <Route path="/app/news" component={EventsContainer} />
                    <Route path="/app/promotions" component={PromotionsContainer} />
                    <Route path="/app/payment" component={PaymentContainer} />
                    <Route path="/app/invoices" component={OpenInvoicesContainer} />
                    <Route path="/app/draftPayment" component={DraftPaymentContainer} />
                    <Route path="/app/register" component={PaymentRegisterContainer} />
                    <Route path="/app/myCards" component={MyCardContainer} />
                    <Route path="/app/rxForm" component={RxFormContainer} />
                    <Route path="/app/estimation" component={RestorationContainer} />
                    <Route path="/app/listReport" component={ListReportContainer} />
                    <Route path="/app/remakeReport" component={RemakeReportContainer} />
                    <Route path="/app/ratingReport" component={RatingReportContainer} />
                    <Route path="/app/ticketReport" component={TicketReportContainer} />
                    <Redirect exact from="/app" to="/app/home" />
                </Switch>
            </main>
        </div>
    );
};

export default LandingPageLayout;
