import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';
import MaterialTable from 'material-table';
import { Column } from 'material-table/types/index';
import { tableIcons } from './DraftCaseLayout';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';

interface Row {
    caseno: string;
    patientname: string;
    casesubmission: string;
    shippingdate: string;
    appointmentdate: string;
    status: string;
}

interface TableState {
    columns: Array<Column<Row>>;
    data: Row[];
}

export interface LiveCaseLayoutProps {

}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
    })
);

const LiveCaseLayout: React.StatelessComponent<LiveCaseLayoutProps> = props => {
    const classes = useStyles({});
    const [state] = React.useState<TableState>({
        columns: [
            {
                title: 'Case No',
                field: 'caseno',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Patient Name',
                field: 'patientname',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Case Submission Date',
                field: 'casesubmission',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Estd.Shipping Date',
                field: 'shippingdate',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Appointment Date',
                field: 'appointmentdate',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Status',
                field: 'status',
                cellStyle: {
                    padding: '10px',
                    fontSize: '13px',
                    textAlign: 'center'
                }
            },
            {
                title: 'Details',
                field: 'fdownload',
                export: false,
                // cellStyle: {
                //   backgroundColor: '#039be5',
                //   color: '#FFF'
                // },
                render: rowData => (
                    // <div style={{ display: 'flex' }}>
                    <Tooltip title="View" placement="top">
                        <IconButton aria-label="view" >
                            <VisibilityIcon fontSize="small" />
                        </IconButton>
                    </Tooltip>
                    // </div>
                ),
                cellStyle: {
                    padding: '0px',
                    textAlign: 'center'
                }
            }
        ],
        data: [
            {
                caseno: '0025',
                patientname: 'VENKATESH BABU',
                casesubmission: '05/26/2020',
                shippingdate: '05/26/2020',
                appointmentdate: '05/29/2020',
                status: 'Processing'
            },
            {
                caseno: '0025',
                patientname: 'VENKATESH BABU',
                casesubmission: '05/26/2020',
                shippingdate: '05/26/2020',
                appointmentdate: '05/29/2020',
                status: 'Processing'
            },
            {
                caseno: '0025',
                patientname: 'VENKATESH BABU',
                casesubmission: '05/26/2020',
                shippingdate: '05/26/2020',
                appointmentdate: '05/29/2020',
                status: 'Processing'
            },
        ],
    });

    return (
        <div className={classes.root}>
            <CssBaseline />
            <MaterialTable
                icons={tableIcons}
                title="Live Cases"
                columns={state.columns}
                data={state.data}
                options={{
                    // filtering: true,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    sorting: true,
                    headerStyle: {
                        backgroundColor: '#29b35aab',
                        color: '#fff',
                        padding: '5px 0px',
                        textAlign: 'center'
                    },
                    exportButton: true,
                    exportAllData: true,
                }}
            />
        </div>
    );
};

export default LiveCaseLayout;
