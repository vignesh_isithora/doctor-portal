import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import CountUp from 'react-countup';

export interface DashboardLayoutProps {

}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            // height: '100vh'

        },
        card1: {
            display: 'flex',
            height: '100%',
            background: 'linear-gradient(45deg, #63B867 10%, transparent 100%)'
        },
        card2: {
            display: 'flex',
            height: '100%',
            background: 'linear-gradient(45deg, #FEA11E 10%, transparent 100%)'
        },
        card3: {
            display: 'flex',
            height: '100%',
            background: 'linear-gradient(45deg, #20AAE1 10%, transparent 100%)'
        },
        details: {
            display: 'flex',
            flexDirection: 'column',
            flexGrow: 1,
        },
        content: {
            // flex: '1 0 auto',
            paddingBottom: '0px'
        },
        cover: {
            width: 150,
            backgroundSize: '100%',
            margin: '10px'
        },
    })
);

const DashboardLayout: React.StatelessComponent<DashboardLayoutProps> = props => {
    const classes = useStyles({});
    return (
        <div className={classes.root}>
            <CssBaseline />
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6} md={4}>
                    <Card className={classes.card1} elevation={5}>
                        <div className={classes.details}>
                            <CardContent className={classes.content}>
                                <Typography component="h5" variant="h5" color="textSecondary">
                                    Open Request
                        </Typography>
                                <Typography variant="h3">
                                    <strong>
                                        <CountUp start={0} end={80} duration={2.75} />
                                    </strong>
                                </Typography>
                            </CardContent>
                        </div>
                        <CardMedia
                            className={classes.cover}
                            image="/assets/request.svg"
                            title="Open Request"
                        />
                    </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={4}>
                    <Card className={classes.card2} elevation={5}>
                        <div className={classes.details}>
                            <CardContent className={classes.content}>
                                <Typography component="h5" variant="h5" color="textSecondary">
                                    Arrive Today
                        </Typography>
                                <Typography variant="h3">
                                    <strong><CountUp start={0} end={50} duration={2.75} /></strong>
                                </Typography>
                            </CardContent>
                        </div>
                        <CardMedia
                            className={classes.cover}
                            image="/assets/today.svg"
                            title="Cases to Arrive Today"
                        />
                    </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={4}>
                    <Card className={classes.card3} elevation={5}>
                        <div className={classes.details}>
                            <CardContent className={classes.content}>
                                <Typography component="h5" variant="h5" color="textSecondary">
                                    Arrive Tommorow
                        </Typography>
                                <Typography variant="h3">
                                    <strong><CountUp start={0} end={100} duration={2.75} /></strong>
                                </Typography>
                            </CardContent>
                        </div>
                        <CardMedia
                            className={classes.cover}
                            image="/assets/tmrw.svg"
                            title="Cases to Arrive Tommorow"
                        />
                    </Card>
                </Grid>
                {/* <Grid item xs={6} sm={3}>
                    <Card className={classes.card}>
                        <div className={classes.details}>
                            <CardContent className={classes.content}>
                                <Typography component="h5" variant="h5">
                                    Live From Space
                        </Typography>
                                <Typography variant="subtitle1" color="textSecondary">
                                    Mac Miller
                        </Typography>
                            </CardContent>
                        </div>
                        <CardMedia
                            className={classes.cover}
                            image="/assets/logo-login.png"
                            title="Live from space album cover"
                        />
                    </Card>
                </Grid> */}
            </Grid>
        </div>
    );
};

export default DashboardLayout;
