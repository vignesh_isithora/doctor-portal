import * as React from 'react';
import { ReactNode } from 'react';
import { Route } from 'react-router-dom';
import { Redirect, Switch } from 'react-router';
import LoginContainer from '../Login/LoginContainer';
import LandingPageContainer from '../LandingPage/LandingPageContainer';
import ErrorContainer from '../../error/ErrorContainer';

class AppViewInitializer extends React.Component {
  render(): ReactNode {
    return (
      <Switch>
        <Route exact path="/login" component={LoginContainer} />
        <Route path="/app" component={LandingPageContainer} />
        <Route exact path="/error" component={ErrorContainer} />
        <Redirect exact from="/" to="/login" />
        <Redirect to="/error" />
      </Switch>
    );
  }
}

export default AppViewInitializer;
