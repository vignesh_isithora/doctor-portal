import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import SmileCaseComponent from './SmileCaseComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const SmileCaseContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(SmileCaseComponent);

export default SmileCaseContainer;
