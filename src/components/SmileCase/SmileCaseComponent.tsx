import * as React from 'react';
import SmileCaseLayout from '../layouts/SmileCaseLayout';

export interface SmileCaseLayoutProps {

}

class SmileCaseComponent extends React.Component<SmileCaseLayoutProps> {
    render() {
        return (
            <div>
                <SmileCaseLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default SmileCaseComponent;
