import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import RatingReportComponent from './RatingReportComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const RatingReportContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(RatingReportComponent);

export default RatingReportContainer;
