import * as React from 'react';
import RatingReportLayout from '../layouts/RatingReportLayout';

export interface RatingReportLayoutProps {

}

class RatingReportComponent extends React.Component<RatingReportLayoutProps> {
    render() {
        return (
            <div>
                <RatingReportLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default RatingReportComponent;
