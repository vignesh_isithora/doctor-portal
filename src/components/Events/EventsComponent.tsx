import * as React from 'react';
import EventsLayout from '../layouts/EventsLayout';

export interface EventsLayoutProps {

}

class EventsComponent extends React.Component<EventsLayoutProps> {
    render() {
        return (
            <div>
                <EventsLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default EventsComponent;
