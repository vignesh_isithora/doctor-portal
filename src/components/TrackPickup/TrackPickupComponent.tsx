import * as React from 'react';
import TrackPickupLayout from '../layouts/TrackPickupLayout';

export interface TrackPickupLayoutProps {

}

class TrackPickupComponent extends React.Component<TrackPickupLayoutProps> {
    render() {
        return (
            <div>
                <TrackPickupLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default TrackPickupComponent;
