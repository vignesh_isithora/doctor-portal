import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import TrackPickupComponent from './TrackPickupComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const TrackPickupContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(TrackPickupComponent);

export default TrackPickupContainer;
