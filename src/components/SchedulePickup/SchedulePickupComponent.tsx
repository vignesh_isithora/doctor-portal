import * as React from 'react';
import SchedulePickupLayout from '../layouts/SchedulePickupLayout';

export interface SchedulePickupLayoutProps {

}

class SchedulePickupComponent extends React.Component<SchedulePickupLayoutProps> {
    render() {
        return (
            <div>
                <SchedulePickupLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default SchedulePickupComponent;
