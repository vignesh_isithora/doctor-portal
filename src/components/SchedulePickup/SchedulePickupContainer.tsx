import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import SchedulePickupComponent from './SchedulePickupComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const SchedulePickupContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(SchedulePickupComponent);

export default SchedulePickupContainer;
