import * as React from 'react';
import AddNewCaseLayout from '../layouts/AddNewCaseLayout';

export interface AddNewCaseLayoutProps {

}

class AddNewCaseComponent extends React.Component<AddNewCaseLayoutProps> {
    render() {
        return (
            <div>
                <AddNewCaseLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default AddNewCaseComponent;
