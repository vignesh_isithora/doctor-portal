import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import AddNewCaseComponent from './AddNewCaseComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const AddNewCaseContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(AddNewCaseComponent);

export default AddNewCaseContainer;
