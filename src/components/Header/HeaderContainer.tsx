import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import HeaderComponent from './HeaderComponent';
import { handleCloseAlert } from 'src/actions/ErrorAction';

export const mapStateToProps = (state: AppState) => {
    return {
        errorTitlePrimary: state.errorState.errorTitlePrimary,
        errorMessagePrimary: state.errorState.errorMessagePrimary,
        errorBarPrimary: state.errorState.errorBarPrimary,
        successMessagePrimary: state.errorState.successMessagePrimary,
        successBarPrimary: state.errorState.successBarPrimary,
    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        handleClose: () => {
            dispatch(handleCloseAlert);
        },
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const HeaderContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(HeaderComponent);

export default HeaderContainer;
