import * as React from 'react';
import HeaderLayout from '../layouts/HeaderLayout';
import Snackbar from '@material-ui/core/Snackbar';
import AlertTitle from '@material-ui/lab/AlertTitle';
import MuiAlert from '@material-ui/lab/Alert';
import { AlertProps } from '@material-ui/lab/Alert';

function Alert(props: AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export interface HeaderLayoutProps {
    errorTitlePrimary: string;
    errorMessagePrimary: string;
    errorBarPrimary: boolean;
    successMessagePrimary: string;
    successBarPrimary: boolean;
    handleClose: () => void;
}

class HeaderComponent extends React.Component<HeaderLayoutProps> {
    render() {
        return (
            <div>
                <HeaderLayout />
                <Snackbar
                    open={this.props.errorBarPrimary}
                    autoHideDuration={2000}
                    onClose={this.props.handleClose}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                >
                    <Alert
                        severity="error"
                    >
                        <AlertTitle>{this.props.errorTitlePrimary}</AlertTitle>
                        {this.props.errorMessagePrimary}
                    </Alert>
                </Snackbar>
                <Snackbar
                    open={this.props.successBarPrimary}
                    autoHideDuration={2000}
                    onClose={this.props.handleClose}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                >
                    <Alert
                        severity="success"
                    >
                        <AlertTitle>{this.props.errorTitlePrimary}</AlertTitle>
                        {this.props.successMessagePrimary}
                    </Alert>
                </Snackbar>
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default HeaderComponent;
