import * as React from 'react';
import LiveCaseLayout from '../layouts/LiveCaseLayout';

export interface LiveCaseLayoutProps {

}

class LiveCaseComponent extends React.Component<LiveCaseLayoutProps> {
    render() {
        return (
            <div>
                <LiveCaseLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default LiveCaseComponent;
