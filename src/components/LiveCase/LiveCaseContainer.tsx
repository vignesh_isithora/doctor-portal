import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import LiveCaseComponent from './LiveCaseComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const LiveCaseContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(LiveCaseComponent);

export default LiveCaseContainer;
