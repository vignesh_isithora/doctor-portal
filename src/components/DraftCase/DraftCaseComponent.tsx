import * as React from 'react';
import DraftCaseLayout from '../layouts/DraftCaseLayout';

export interface DraftCaseLayoutProps {

}

class DraftCaseComponent extends React.Component<DraftCaseLayoutProps> {
    render() {
        return (
            <div>
                <DraftCaseLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default DraftCaseComponent;
