import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import DraftCaseComponent from './DraftCaseComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const DraftCaseContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(DraftCaseComponent);

export default DraftCaseContainer;
