import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import RemakeReportComponent from './RemakeReportComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const RemakeReportContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(RemakeReportComponent);

export default RemakeReportContainer;
