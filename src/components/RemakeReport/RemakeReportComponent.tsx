import * as React from 'react';
import RemakeReportLayout from '../layouts/RemakeReportLayout';

export interface RemakeReportLayoutProps {

}

class RemakeReportComponent extends React.Component<RemakeReportLayoutProps> {
    render() {
        return (
            <div>
                <RemakeReportLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default RemakeReportComponent;
