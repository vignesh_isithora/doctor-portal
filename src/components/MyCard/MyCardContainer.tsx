import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import MyCardComponent from './MyCardComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const MyCardContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(MyCardComponent);

export default MyCardContainer;
