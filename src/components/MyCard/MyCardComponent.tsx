import * as React from 'react';
import MyCardLayout from '../layouts/MyCardLayout';

export interface MyCardLayoutProps {

}

class MyCardComponent extends React.Component<MyCardLayoutProps> {
    render() {
        return (
            <div>
                <MyCardLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default MyCardComponent;
