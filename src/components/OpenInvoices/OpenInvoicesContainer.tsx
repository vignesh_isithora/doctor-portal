import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import OpenInvoicesComponent from './OpenInvoicesComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const OpenInvoicesContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(OpenInvoicesComponent);

export default OpenInvoicesContainer;
