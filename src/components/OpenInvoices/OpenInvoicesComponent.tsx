import * as React from 'react';
import OpenInvoicesLayout from '../layouts/OpenInvoicesLayout';

export interface OpenInvoicesLayoutProps {

}

class OpenInvoicesComponent extends React.Component<OpenInvoicesLayoutProps> {
    render() {
        return (
            <div>
                <OpenInvoicesLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default OpenInvoicesComponent;
