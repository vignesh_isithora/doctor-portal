import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import PromotionsComponent from './PromotionsComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const PromotionsContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(PromotionsComponent);

export default PromotionsContainer;
