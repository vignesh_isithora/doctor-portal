import * as React from 'react';
import PromotionsLayout from '../layouts/PromotionsLayout';

export interface PromotionsLayoutProps {

}

class PromotionsComponent extends React.Component<PromotionsLayoutProps> {
    render() {
        return (
            <div>
                <PromotionsLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default PromotionsComponent;
