import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import DraftPaymentComponent from './DraftPaymentComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const DraftPaymentContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(DraftPaymentComponent);

export default DraftPaymentContainer;
