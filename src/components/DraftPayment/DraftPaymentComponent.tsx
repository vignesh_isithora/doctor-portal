import * as React from 'react';
import DraftPaymentLayout from '../layouts/DraftPaymentLayout';

export interface DraftPaymentLayoutProps {

}

class DraftPaymentComponent extends React.Component<DraftPaymentLayoutProps> {
    render() {
        return (
            <div>
                <DraftPaymentLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default DraftPaymentComponent;
