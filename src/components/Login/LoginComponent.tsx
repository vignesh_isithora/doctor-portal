import * as React from 'react';
import LoginLayout from '../layouts/LoginLayout';

export interface LoginLayoutProps {
    onSubmit: (username: string, password: string) => void;
}

class LoginComponent extends React.Component<LoginLayoutProps> {
    render() {
        return (
            <div>
                <LoginLayout
                    onSubmit={this.props.onSubmit}
                />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default LoginComponent;
