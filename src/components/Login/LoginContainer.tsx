import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import LoginComponent from './LoginComponent';
import { doAuthentication } from 'src/actions/LoginAction';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        onSubmit: (username: string, password: string) => {
            // dispatch(fetchCurrentUser);
            dispatch(doAuthentication(username, password));
        },
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const LoginContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(LoginComponent);

export default LoginContainer;
