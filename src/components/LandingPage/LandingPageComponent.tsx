import * as React from 'react';
import LandingPageLayout from '../layouts/LandingPageLayout';

export interface LandingPageProps {
    navTo: (nav: string) => void;
}

class LandingPageComponent extends React.Component<LandingPageProps> {
    render() {
        return (
            <div>
                <LandingPageLayout
                    navTo={this.props.navTo}
                />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default LandingPageComponent;
