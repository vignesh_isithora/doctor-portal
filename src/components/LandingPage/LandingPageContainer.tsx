import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import LandingPageComponent from './LandingPageComponent';
import { navigationPage } from 'src/actions/LandingPageAction';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        navTo: (nav: string) => {
            dispatch(navigationPage(nav));
        }
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const LandingPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(LandingPageComponent);

export default LandingPageContainer;
