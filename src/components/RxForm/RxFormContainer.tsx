import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import RxFormComponent from './RxFormComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const RxFormContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(RxFormComponent);

export default RxFormContainer;
