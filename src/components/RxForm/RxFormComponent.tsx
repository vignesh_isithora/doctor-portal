import * as React from 'react';
import RxFormLayout from '../layouts/RxFormLayout';

export interface RxFormLayoutProps {

}

class RxFormComponent extends React.Component<RxFormLayoutProps> {
    render() {
        return (
            <div>
                <RxFormLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default RxFormComponent;
