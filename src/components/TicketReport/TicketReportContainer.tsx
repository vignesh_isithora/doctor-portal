import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import TicketReportComponent from './TicketReportComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const TicketReportContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(TicketReportComponent);

export default TicketReportContainer;
