import * as React from 'react';
import TicketReportLayout from '../layouts/TicketReportLayout';

export interface TicketReportLayoutProps {

}

class TicketReportComponent extends React.Component<TicketReportLayoutProps> {
    render() {
        return (
            <div>
                <TicketReportLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default TicketReportComponent;
