import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import DashboardComponent from './DashboardComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const DashboardContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(DashboardComponent);

export default DashboardContainer;
