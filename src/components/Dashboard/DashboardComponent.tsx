import * as React from 'react';
import DashboardLayout from '../layouts/DashboardLayout';

export interface DashboardLayoutProps {

}

class DashboardComponent extends React.Component<DashboardLayoutProps> {
    render() {
        return (
            <div>
                <DashboardLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default DashboardComponent;
