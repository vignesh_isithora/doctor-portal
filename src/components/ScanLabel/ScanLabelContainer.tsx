import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import ScanLabelComponent from './ScanLabelComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const ScanLabelContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ScanLabelComponent);

export default ScanLabelContainer;
