import * as React from 'react';
import ScanLabelLayout from '../layouts/ScanLabelLayout';

export interface ScanLabelLayoutProps {

}

class ScanLabelComponent extends React.Component<ScanLabelLayoutProps> {
    render() {
        return (
            <div>
                <ScanLabelLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default ScanLabelComponent;
