import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import RestorationComponent from './RestorationComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const RestorationContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(RestorationComponent);

export default RestorationContainer;
