import * as React from 'react';
import RestorationLayout from '../layouts/RestorationLayout';

export interface RestorationLayoutProps {

}

class RestorationComponent extends React.Component<RestorationLayoutProps> {
    render() {
        return (
            <div>
                <RestorationLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default RestorationComponent;
