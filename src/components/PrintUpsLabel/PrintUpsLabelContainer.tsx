import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import PrintUpsLabelComponent from './PrintUpsLabelComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const PrintUpsLabelContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(PrintUpsLabelComponent);

export default PrintUpsLabelContainer;
