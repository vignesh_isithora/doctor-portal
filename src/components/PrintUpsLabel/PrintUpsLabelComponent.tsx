import * as React from 'react';
import PrintUpsLabelLayout from '../layouts/PrintUpsLabelLayout';

export interface PrintUpsLabelLayoutProps {

}

class PrintUpsLabelComponent extends React.Component<PrintUpsLabelLayoutProps> {
    render() {
        return (
            <div>
                <PrintUpsLabelLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default PrintUpsLabelComponent;
