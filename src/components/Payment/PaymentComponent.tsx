import * as React from 'react';
import PaymentLayout from '../layouts/PaymentLayout';

export interface PaymentLayoutProps {

}

class PaymentComponent extends React.Component<PaymentLayoutProps> {
    render() {
        return (
            <div>
                <PaymentLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default PaymentComponent;
