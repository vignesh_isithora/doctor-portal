import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import PaymentComponent from './PaymentComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const PaymentContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(PaymentComponent);

export default PaymentContainer;
