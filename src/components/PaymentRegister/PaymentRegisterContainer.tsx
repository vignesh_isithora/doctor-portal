import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import PaymentRegisterComponent from './PaymentRegisterComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const PaymentRegisterContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(PaymentRegisterComponent);

export default PaymentRegisterContainer;
