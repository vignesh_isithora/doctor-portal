import * as React from 'react';
import PaymentRegisterLayout from '../layouts/PaymentRegisterLayout';

export interface PaymentRegisterLayoutProps {

}

class PaymentRegisterComponent extends React.Component<PaymentRegisterLayoutProps> {
    render() {
        return (
            <div>
                <PaymentRegisterLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default PaymentRegisterComponent;
