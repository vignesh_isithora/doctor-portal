import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import ShippingCaseComponent from './ShippingCaseComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const ShippingCaseContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ShippingCaseComponent);

export default ShippingCaseContainer;
