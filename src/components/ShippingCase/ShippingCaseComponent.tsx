import * as React from 'react';
import ShippingCaseLayout from '../layouts/ShippingCaseLayout';

export interface ShippingCaseLayoutProps {

}

class ShippingCaseComponent extends React.Component<ShippingCaseLayoutProps> {
    render() {
        return (
            <div>
                <ShippingCaseLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default ShippingCaseComponent;
