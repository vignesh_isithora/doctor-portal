import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import GetLabelComponent from './GetLabelComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const GetLabelContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(GetLabelComponent);

export default GetLabelContainer;
