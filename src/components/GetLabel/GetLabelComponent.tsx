import * as React from 'react';
import GetLabelLayout from '../layouts/GetLabelLayout';

export interface GetLabelLayoutProps {

}

class GetLabelComponent extends React.Component<GetLabelLayoutProps> {
    render() {
        return (
            <div>
                <GetLabelLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default GetLabelComponent;
