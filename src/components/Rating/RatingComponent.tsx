import * as React from 'react';
import RatingLayout from '../layouts/RatingLayout';

export interface RatingLayoutProps {

}

class RatingComponent extends React.Component<RatingLayoutProps> {
    render() {
        return (
            <div>
                <RatingLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default RatingComponent;
