import * as React from 'react';
import ListReportLayout from '../layouts/ListReportLayout';

export interface ListReportLayoutProps {

}

class ListReportComponent extends React.Component<ListReportLayoutProps> {
    render() {
        return (
            <div>
                <ListReportLayout />
            </div>

        );
    }
    // componentDidMount() {
    // //   this.props.onComponentDidMount();
    // }
}

export default ListReportComponent;
