import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import ListReportComponent from './ListReportComponent';

export const mapStateToProps = (state: AppState) => {
    return {

    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        // onComponentDidMount: () => {
        // dispatch(clearSession);
        // }
    };
};

const ListReportContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListReportComponent);

export default ListReportContainer;
