import { AnyAction, Reducer } from 'redux';
export const STORE_ERROR_TITLE_PRIMARY = 'error/errorTitle';
export const STORE_ERROR_MESSAGE_PRIMARY = 'error/errorMessage';
export const STORE_ERROR_BAR_PRIMARY = 'error/errorBar';
export const STORE_ERROR_DETAILS = 'error/errorDetails';
export const STORE_SUCCESS_TITLE = 'success/successTitle';
export const STORE_SUCCESS_MESSAGE_PRIMARY = 'success/successMessage';
export const STORE_SUCCESS_BAR_PRIMARY = 'success/successBar';
export const STORE_ERROR_FILES_UPLOADED = 'error/unsupportedformat';
export const STORE_SUCCESS_FILES_UPLOADED = 'success/addedSuccessfully';
export const STORE_WARN_FILES_UPLOADED = 'warning/dupicates-detected';
export const STORE_WARN_FILES_SUBMIT = 'warning/submit-condition';
export const STORE_SUCCESS_SUBMIT = 'success/submit-success';
export const STORE_SUCCESS_SUBMIT_MSG = 'success/submit-success-messege';
export const STORE_ERROR_SUBMIT = 'error/submit-error';
export const STORE_ERROR_SUBMIT_MSG = 'error/submit-error-messege';
export const STORE_SUCCESS_UPDATE = 'success/Update-success';
export const STORE_SUCCESS_UPDATE_MSG = 'success/Update-success-messege';
export const STORE_ERROR_UPDATE = 'error/Update-error';
export const STORE_ERROR_CONCURRENT = 'error/concurrent-user-error';
export const STORE_ERROR_UPDATE_MSG = 'error/Update-error-messege';
export const STORE_ERROR_CONCURRENT_MSG = 'error/concurrent-error-messege';

export const STORE_ERROR_FILES_UPLOADED_MSG = 'error/error-message';

export interface ErrorState {
    errorTitlePrimary?: String;
    errorMessagePrimary?: String;
    errorBarPrimary?: boolean;
    successMessagePrimary?: String;
    successBarPrimary?: boolean;
}

const initialState: ErrorState = {
    errorTitlePrimary: '',
    errorMessagePrimary: '',
    errorBarPrimary: false,
    successMessagePrimary: '',
    successBarPrimary: false

};

const reducer: Reducer<ErrorState> =
    (state: ErrorState = initialState, action: AnyAction): ErrorState => {
        switch (action.type) {
            case STORE_ERROR_TITLE_PRIMARY:
                return {
                    ...state,
                    errorTitlePrimary: action.errorTitlePrimary
                };
            case STORE_ERROR_BAR_PRIMARY:
                return {
                    ...state,
                    errorBarPrimary: action.errorBarPrimary
                };
            case STORE_ERROR_MESSAGE_PRIMARY:
                return {
                    ...state,
                    errorMessagePrimary: action.errorMessagePrimary
                };
            case STORE_SUCCESS_BAR_PRIMARY:
                return {
                    ...state,
                    successBarPrimary: action.successBarPrimary
                };
            case STORE_SUCCESS_MESSAGE_PRIMARY:
                return {
                    ...state,
                    successMessagePrimary: action.successMessagePrimary
                };
            default:
                return state;
        }
    };
export default reducer;
