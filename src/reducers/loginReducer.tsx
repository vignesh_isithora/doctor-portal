import { AnyAction, Reducer } from 'redux';

export const STORE_USERNAME = 'login/store-username';
export const STORE_PASSWORD = 'login/store-password';

export interface LoginState {
    username: string;
    password: string;
}

const initialState: LoginState = {
    username: '',
    password: ''
};

const reducer: Reducer<LoginState> = (state: LoginState = initialState, action: AnyAction): LoginState => {
    switch (action.type) {
        case STORE_USERNAME:
            return {
                ...state,
                username: action.username
            };
        case STORE_PASSWORD:
            return {
                ...state,
                password: action.password
            };
        default:
            return state;
    }
};

export default reducer;
